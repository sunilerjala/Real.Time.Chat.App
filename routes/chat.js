var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var user = require('../models/User.js');
var passport = require('passport');
require('../config/passport')(passport);
var chat = require('../models/Chat.js')



// get msgs between users
router.get('/:id', function(req, res, next) {
    var chatIdOne = req.params.id
    var chatIdTwo = req.params.id.slice(24,48)+req.params.id.slice(0,24)
    console.log('chat id1'+chatIdOne)
    chat.find({ chatId: { $in : [chatIdOne,chatIdTwo ]} }, (err, messages) => {
      if(err) throw err;
      res.json(messages);
  });
  });
  

  // router.get('/:id', passport.authenticate('jwt', { session: false}), function(req, res) {
  
  //   var token = getToken(req.headers);
  //   if (token) {
  //     var chatIdOne = req.params.id
  //     var chatIdTwo = req.params.id.slice(24,48)+req.params.id.slice(0,24)
  //     console.log('chat id1'+chatIdOne)
  //     chat.find({ chatId: { $in : [chatIdOne,chatIdTwo ]} }, (err, messages) => {
  //       if(err) throw err;
  //       res.json(messages);
  //   });
  //     }
    
  //    else {
       
  //     return res.status(401).send({success: false, msg: 'Unauthorized.'});
  //   }
  // });





  // router.post('/:id', passport.authenticate('jwt', { session: false}), function(req, res) {
  
  //   var token = getToken(req.headers);
  //   if (token) {
  //     var newMessage = new chat({
  //       fromId: req.body.fromId,
  //       toId: req.body.toId,
  //       msg:req.body.message,
  //       fromName:req.body.fromName,
  //       toName:req.body.toName,
  //       chatId:req.body.chatId
        
  //     });
      
  //     newMessage.save(function(err) {
  //       if (err) {
  //         return res.json({success: false, msg: 'Username already exists.'});
  //       }
  //       res.json({success: true, msg: 'Successful created new user.'});
  //     });


       
  //     }
    
  //    else {
       
  //     return res.status(401).send({success: false, msg: 'Unauthorized.'});
  //   }
  // });


  //saving msg in database 
  router.post('/:id', function(req, res) {
      
    var newMessage = new chat({
        fromId: req.body.fromId,
        toId: req.body.toId,
        msg:req.body.message,
        fromName:req.body.fromName,
        toName:req.body.toName,
        chatId:req.body.chatId
        
      });
      
      newMessage.save(function(err) {
        if (err) {
          return res.json({success: false, msg: 'Username already exists.'});
        }
        res.json({success: true, msg: 'Successful created new user.'});
      });
      
  });
  



  //authentication  
  getToken = function (headers) {
    if (headers && headers.authorization) {
      var parted = headers.authorization.split(' ');
      if (parted.length === 2) {
        console.log(parted[1])
        return parted[1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  };
  



  router.get('/', passport.authenticate('jwt', { session: false}), function(req, res) {
  
    var token = getToken(req.headers);
    if (token) {
            user.find({}, (err, users) => {
            if(err) throw err;
            res.json(users);
        });
      }
    
     else {
       
      return res.status(401).send({success: false, msg: 'Unauthorized.'});
    }
  });
  
  
  module.exports = router;
