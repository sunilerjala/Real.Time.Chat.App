var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var user = require('../models/User.js');
var passport = require('passport');
require('../config/passport')(passport);

/* GET User */
router.get('/', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
          user.find({}, (err, users) => {
          if(err) throw err;
          res.json(users);
      });
    }
   else {
    return res.status(401).send({success: false, msg: 'Unauthorized.'});
  }
});



getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      console.log(parted[1])
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};


//get chat messages between them between them

router.get('/:id', function(req, res, next) {
  user.findById(req.params.id.slice(24,48), function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});


// router.put('/:id', function(req, res, next) {
//   user.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
//     console.log(req.body);
//     console.log(req.params);
//     if (err) return next(err);
//     res.json(post);
//   });
// });



module.exports = router;
