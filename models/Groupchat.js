var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var GroupChatSchema = new Schema({
    
    fromName: {
          type: String,
          required: true
      },
    msg:{
        type:String,
        required:true
    }
  });
  

  module.exports = mongoose.model('Groupchat', GroupChatSchema);