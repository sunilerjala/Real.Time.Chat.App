var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var ChatSchema = new Schema({
    chatId:{
        type:String,
        required:true
    },
    fromId: {
          type: String,
          required: true
      },
    fromName:{
        type:String,
        required:true
    },
    toId: {
          type: String,
          required: true
      },
    toName:{
          type:String,
          required:true
      },
    msg:{
        type:String,
        required:true
    }
  });
  

  module.exports = mongoose.model('Chat', ChatSchema);