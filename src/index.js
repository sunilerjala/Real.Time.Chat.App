import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Login from './components/Login';
import Register from './components/Register';
import Chat from './components/Chat';
import Groupchat from './components/Groupchat'

const NotFound = () => {
  <div>
  Not Found
  </div>
}

ReactDOM.render(
  <Router>
      <Switch>
        <Route exact path='/' component={App} />
        <Route path='/login' component={Login} />
        <Route path='/register' component={Register} />
        <Route path='/chat/:id' component={Chat} />
        <Route path='/not' component={NotFound} />
        <Route path='/groupchat/:id' component = {Groupchat}/>
      </Switch>
      
  </Router>,
  document.getElementById('root')
);
registerServiceWorker();
