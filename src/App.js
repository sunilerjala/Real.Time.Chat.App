import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],
      curUser:{}
    };
  }

  componentDidMount() {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwtToken');
    axios.get('/api/chat')
      .then(res => {
        console.log(res.data);
        this.setState({ users: res.data ,curUser: JSON.parse(localStorage.getItem('user'))});
      })
      .catch((error) => {
        if(error.response.status === 401) {
          this.props.history.push("/login");
        }
      });
  }


  //logout page
  logout = () => {
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('user');
    window.location.reload();
  }

  //render page
  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
               &nbsp;
              {localStorage.getItem('jwtToken') &&

                <button class="btn btn-primary" onClick={this.logout}>Logout</button>
              }
            </h3>
            <h3 class="panel-title"> Welcome {this.state.curUser.username}</h3>
          </div>
          <div class="panel-body">
         
            <table class="table table-stripe">
              <thead>
                <tr>
                  
                  <th>UserName</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {this.state.users.map(user =>
                  (this.state.curUser.username !== user.username)?
                  <tr>
                    <td>{user.username}</td>
                    <Link to={`/chat/${this.state.curUser._id+user._id}`} class="btn btn-success">Chat</Link>
                  </tr>:undefined
                )}
              </tbody>
            </table>
            <Link to={`/groupchat/${this.state.curUser._id}`} class="btn btn-success">Group Chat</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
