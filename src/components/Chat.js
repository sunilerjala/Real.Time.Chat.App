import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './Chat.css';

class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      curUser:{},
      message:'',
      messages:[],
      fun:''
    };
  }


  
  
  componentDidMount() {
    
    axios.get('/api/user/'+this.props.match.params.id)
      .then(res => {
        this.setState({ user: res.data,curUser: JSON.parse(localStorage.getItem('user'))});
      });
      
    axios.get('/api/chat/'+this.props.match.params.id)
    .then(res =>{
      console.log(res.data)
      this.setState({messages:res.data})
    });
    

  }

  componentDidUpdate() {
    
    axios.get('/api/chat/'+this.props.match.params.id)
    .then(res =>{
        // if(this.state.messages.length !== res.data.length ) {
        this.setState({messages:res.data})
        
    });

  }



  onChange = (e) => {
    e.target.name = e.target.value;
    this.setState({message:e.target.name})
  }



  //submitting into database
  onSubmit = (e) => {
   
    this.setState({message:''})
    
    e.preventDefault();
    const fromName = this.state.curUser.username
    const fromId = this.state.curUser._id
    const toId = this.state.user._id
    const toName = this.state.user.username
    const message  = this.state.message
    const chatId = this.state.curUser._id+this.state.user._id
    
    
    axios.post('/api/chat/'+this.props.match.params.id, { fromId,fromName,toId,toName,toId,message,chatId })
      .then((result) => {
        // this.props.history.push("/")
        // window.location.reload();
      });
  }





  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Chat with User
            </h3>
          </div>
          <div class="panel-body">
            <h4><Link to={`/`}><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> User List</Link></h4>
            <h5>Chat With {this.state.user.username}</h5>
            {this.state.messages.map(txt =>
                  
                  (this.state.curUser._id === txt.fromId)?
                    
                    <div class="container darker">
                          <p><b>You</b></p>
                          <p>{txt.msg}</p>
                          {/* <span class="time-right"></span> */}
                   </div>
                   :
                   <div class="container">
                          <p><b>{txt.fromName}</b></p>
                          <p>{txt.msg}</p>
                          {/* <span class="time-left"></span> */}
                   </div>

            )}
            <form class="form-signin" onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="username">message</label>
                <input type="text" class="form-control" name="message" value={this.state.message?this.state.message:''} onChange={this.onChange} required placeholder="send message" />
              </div>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Chat;