import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './Groupchat.css';

class Groupchat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      curUser:{},
      message:'',
      messages:[]
    };
  }


  
  componentDidMount() {
    
    // axios.get('/api/user/'+this.props.match.params.id)
    //   .then(res => {
    //     this.setState({ user: res.data,curUser: JSON.parse(localStorage.getItem('user'))});
    //   });
      
    axios.get('/api/groupchat/'+this.props.match.params.id)
    .then(res =>{
      console.log(res.data)
      this.setState({messages:res.data,curUser: JSON.parse(localStorage.getItem('user'))})
    });
    

  }

  componentDidUpdate() {
    
    axios.get('/api/groupchat/'+this.props.match.params.id)
    .then(res =>{
        // if(this.state.messages.length !== res.data.length ) {
        this.setState({messages:res.data})
        
    });

  }



  onChange = (e) => {
    e.target.name = e.target.value;
    this.setState({message:e.target.name})
  }



  //submitting into database
  onSubmit = (e) => {
   
    this.setState({message:''})
    
    e.preventDefault();
    
    const fromName = this.state.curUser.username
    const message  = this.state.message
    console.log(fromName+message)
    
    
    axios.post('/api/groupchat/'+this.props.match.params.id, { fromName,message})
      .then((result) => {
        // this.props.history.push("/")
        // window.location.reload();
      });
  }





  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Chat with Group
            </h3>
          </div>
          <div class="panel-body">
            <h4><Link to={`/`}><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Back with Users Chat</Link></h4>
            
            {this.state.messages.map(txt =>
                  
                  (this.state.curUser.username === txt.fromName)?
                    
                    <div class="container darker">
                          <p><b>You</b></p>
                          <p>{txt.msg}</p>
                          {/* <span class="time-right"></span> */}
                   </div>
                   :
                   <div class="container">
                          <p><b>{txt.fromName}</b></p>
                          <p>{txt.msg}</p>
                          {/* <span class="time-left"></span> */}
                   </div>

            )}
            <form class="form-signin" onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="username">message</label>
                <input type="text" class="form-control" name="message" value={this.state.message?this.state.message:''} onChange={this.onChange} required placeholder="send message" />
              </div>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Groupchat;